module gitlab.com/bassforce86/webapp

go 1.14

require (
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/gosimple/slug v1.9.0
	github.com/lib/pq v1.7.1
	gopkg.in/yaml.v2 v2.3.0
)
