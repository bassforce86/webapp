package routing

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"time"

	pages "gitlab.com/bassforce86/webapp/app/page"
	"gitlab.com/bassforce86/webapp/app/resources"

	"github.com/gosimple/slug"
)

var templates = template.Must(template.ParseGlob("app/templates/*.html"))
var isValid = regexp.MustCompile("(?:/(\\w+)+)+(?:\\.(css|ico))*")

func renderTemplate(w http.ResponseWriter, tmpl string, p *pages.Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func renderPage(w http.ResponseWriter, tmpl string, id int64) {
	p, err := pages.FetchPage(id)
	if err != nil {
		log.Printf(err.Error())
		ErrorPageHandler(w, http.StatusNotFound)
	}
	renderTemplate(w, tmpl, p)
}

func RenderResource(w http.ResponseWriter, src string, ext string) {
	resource, err := resources.Fetch(src, ext)
	if err != nil {
		ErrorResourceHandler(w, http.StatusNotFound)
		return
	}
	if ext == "css" {
		w.Header().Set("Content-Type", "text/css; charset=utf-8")
	}
	if ext == "ico" {
		w.Header().Set("Content-Type", "image/x-icon")
	}
	if ext == "js" {
		w.Header().Set("Content-Type", "application/javascript; charset=utf-8")
	}
	_, _ = fmt.Fprint(w, string(resource.File))
	return
}

func PageHandler(fn func(http.ResponseWriter, *http.Request, int64)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("route: ", r.URL.Path)
		if r.URL.Path == "/" {
			fn(w, r, 1)
			return
		}

		m := isValid.FindStringSubmatch(r.URL.Path)
		if m == nil {
			ErrorPageHandler(w, http.StatusNotFound)
			return
		}
		id, err := strconv.Atoi(m[1])
		if err != nil {
			ErrorPageHandler(w, http.StatusNotFound)
			return
		}
		fn(w, r, int64(id))
	}
}
func ResourceHandler(fn func(w http.ResponseWriter, file string, ext string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := isValid.FindStringSubmatch(r.URL.Path)
		if m == nil {
			log.Println("route: ", r.URL.Path)
			ErrorResourceHandler(w, http.StatusNotFound)
			return
		}
		fn(w, m[1], m[2])
	}
}

func ErrorPageHandler(w http.ResponseWriter, status int) {
	if status == http.StatusNotFound {
		w.WriteHeader(status)
		page := &pages.Page{PageId: 404, Name: "404", Body: "Sorry, not found", CreatedAt: time.Now(), UpdatedAt: time.Now()}
		renderTemplate(w, "404", page)
	}
}
func ErrorResourceHandler(w http.ResponseWriter, status int) {
	w.WriteHeader(status)
	return
}

//noinspection GoUnusedParameter
func ViewHandler(w http.ResponseWriter, r *http.Request, id int64) {
	renderPage(w, "view", id)
}

//noinspection GoUnusedParameter
func EditHandler(w http.ResponseWriter, r *http.Request, id int64) {
	renderPage(w, "edit", id)
}

//noinspection GoUnusedParameter
func CreateHandler(w http.ResponseWriter, r *http.Request, id int64) {
	p, err := pages.CreatePage(id)
	if err != nil {
		log.Printf(err.Error())
		ErrorPageHandler(w, http.StatusNotFound)
	}
	renderTemplate(w, "create", p)
}
func SaveHandler(w http.ResponseWriter, r *http.Request, id int64) {
	parentId, err := strconv.Atoi(r.FormValue("parent"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	parent := int64(parentId)
	name := r.FormValue("name")
	body := r.FormValue("body")
	newSlug := r.FormValue("slug")

	p := &pages.Page{PageId: id, Name: name, Slug: slug.Make(newSlug), Body: body, Parent: parent}
	err = p.SavePage()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/"+strconv.FormatInt(p.PageId, 10), http.StatusFound)
}
func DeleteHandler(w http.ResponseWriter, r *http.Request, id int64) {
	p, err := pages.FetchPage(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = p.DeletePage()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
