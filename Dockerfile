FROM golang:latest

RUN mkdir -p /go/src/webapp
WORKDIR /go/src/webapp

ADD . /go/src/webapp

RUN go get -u -v \
github.com/golang-migrate/migrate \
gopkg.in/yaml.v2 \
github.com/lib/pq \
github.com/gosimple/slug \
github.com/hashicorp/go-multierror