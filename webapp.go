package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/bassforce86/webapp/app/routing"
	"gitlab.com/bassforce86/webapp/db"
)

// Environment struct
type Environment struct {
	Name string
	Port string
	Host string
}

func configureEnvironment() Environment {
	env := os.Getenv("env")
	if env == "" {
		env = "local"
	}
	port := os.Getenv("port")
	if port == "" {
		port = "8080"
	}
	host := os.Getenv("host")
	if host == "" || env == "local" {
		host = "http://localhost"
	}

	return Environment{Name: env, Port: port, Host: host}
}

func main() {
	env := configureEnvironment()
	log.Printf("Application Environment: %s", env.Name)
	log.Printf("Application starting up: %s:%s", env.Host, env.Port)
	db.Setup(env.Name)
	http.HandleFunc("/create/", routing.PageHandler(routing.CreateHandler))
	http.HandleFunc("/edit/", routing.PageHandler(routing.EditHandler))
	http.HandleFunc("/save/", routing.PageHandler(routing.SaveHandler))
	http.HandleFunc("/delete/", routing.PageHandler(routing.DeleteHandler))
	http.HandleFunc("/resources/", routing.ResourceHandler(routing.RenderResource))
	http.HandleFunc("/", routing.PageHandler(routing.ViewHandler))
	if env.Name != "production" {
		err := db.RunMigrations()
		if err != nil {
			log.Fatal(err)
		}
	}
	log.Fatal(http.ListenAndServe(":"+env.Port, nil))
}
