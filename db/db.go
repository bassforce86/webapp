package db

import (
	"database/sql"
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

type Database struct {
	Db Details `yaml:"db"`
}

type Details struct {
	Host     string `yaml:"host"`
	Port     int16  `yaml:"port"`
	User     string `yaml:"username"`
	Password string `yaml:"password"`
	Name     string `yaml:"name"`
}

var ConnectionString string
var ConnectionURL string

func Setup(environment string) {
	name := "environments/" + environment + ".yaml"
	config, err := os.Open(name)
	if err != nil {
		log.Println(err)
		return
	}
	defer config.Close()

	b, _ := ioutil.ReadAll(config)
	var env Database
	err = yaml.Unmarshal([]byte(b), &env)
	if err != nil {
		log.Println(err)
		return
	}

	Connection := &env.Db

	log.Printf("Database: -host [%s] -port [:%d]", Connection.Host, Connection.Port)

	ConnectionString = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		Connection.Host, Connection.Port, Connection.User, Connection.Password, Connection.Name)

	ConnectionURL = fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
		Connection.User, Connection.Password, Connection.Host, Connection.Port, Connection.Name)
}

func RunMigrations() error {
	db, err := sql.Open("postgres", ConnectionURL)
	if err != nil {
		return err
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return err
	}
	m, err := migrate.NewWithDatabaseInstance(
		"file:///go/src/webapp/db/migrations",
		"postgres", driver)
	if err != nil {
		return err
	}
	m.Up()
	return nil
}
